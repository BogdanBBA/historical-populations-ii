program HP2;

uses
  Vcl.Forms,
  HP2pas in 'HP2pas.pas' {F1},
  DataTypes in 'DataTypes.pas',
  Vcl.Themes,
  Vcl.Styles,
  Settings in 'Settings.pas' {FSett};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Iceberg Classico');
  Application.CreateForm(TF1, F1);
  Application.CreateForm(TFSett, FSett);
  Application.Run;
end.
