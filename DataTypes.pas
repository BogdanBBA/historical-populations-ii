unit DataTypes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ShellAPI, Menus, ExtCtrls, jpeg, PngImage, ImgList,
  ComCtrls, MPlayer, ToolWin, XML.VerySimple, UIntList;

const
  nl=#13#10; dnl=nl+nl;
  KB=1024; MB=KB*KB;
  NFo: array[0..1] of string=('data', 'data\backups');
  NFi: array[0..0] of string=('data\settings.xml');

type TPopulation=record
  Year: word;
  Value: longword;
end;

type TPlace=record
  Nume: string;
  Color: TColor;
  Tags: TStringList;
  nPop: word;
  Populations: array of TPopulation;
end;

var
  pl: array of TPlace;
  Tags: TXMLNode;

  selPl: TStringList;
  Years, selYears: TIntList;
  fromY, toY: word;

  nPl: word;

  Sett, X: TXMLVerySimple;
  f: textfile;

procedure log(s: string);
function pozPl(s: string): integer;
procedure ReadData;

implementation

uses HP2pas, FunctiiBaza;

procedure log(s: string);
var logf: textfile;
begin
  try
    F1.logL.Caption:=s+formatdatetime(' (d mmm yyyy, hh:nn)', now);
    assignfile(logf, 'data\log.txt'); append(logf); writeln(logf, formatdatetime('dd.mmm.yyyy hh:nn:ss    |    ', now), s); closefile(logf)
  except
    begin
      try
        F1.logL.Caption:=s+formatdatetime('"(Log retry, "d mmm yyyy, hh:nn)', now);
        assignfile(logf, 'data\log.txt'); append(logf); writeln(logf, formatdatetime('dd.mmm.yyyy hh:nn:ss    |    "Log retry: "', now), s); closefile(logf)
      except on E:Exception do MessageDlg('Failed a second time to log message "'+s+'"'+dnl+e.classname+' :  '+e.Message, mtError, [mbOK], 0) end
    end
  end
end;

function pozPl(s: string): integer;
var i: word;
begin
  result:=-1; if nPl=0 then exit;
  for i:=0 to nPl-1 do if ansiuppercase(pl[i].Nume)=ansiuppercase(s) then
    begin result:=i; exit end
end;

procedure DecodeTags(s: string; var x: TStringList);
begin
  try
  if x=nil then x:=TStringList.Create;
  while s<>'' do begin x.Add(copy(s, 1, pos(';', s)-1)); delete(s, 1, pos(';', s)) end
  except on E:Exception do MessageDlg('DecodeTags(s="'+s+'", x) ERROR'+dnl+e.classname+' :  '+e.Message, mtError, [mbOK], 0) end
end;

function EncodeTags(x: TStringList): string;
var i: word; rs: string;
begin
  rs:=''; if x.Count>0 then for i:=0 to x.Count-1 do rs:=rs+x[i]+';'; result:=rs
end;

procedure ReadData;
var i, j: word; xl, yl: TXMLNodeList;
begin
  for i:=0 to high(NFo) do if not directoryexists(NFo[i]) then begin showmessage('The folder "'+NFo[i]+'" does not exist. That is a big error.'+dnl+'You probably have a faulty installation, so reinstall. The app will now close.'); Halt end;
  for i:=0 to high(NFi) do if not fileexists(NFi[i]) then begin showmessage('The file "'+NFi[i]+'" does not exist. That is a big error.'+dnl+'You probably have a faulty installation, so reinstall. The app will now close.'); Halt end;
  with FormatSettings do begin DecimalSeparator:='.'; ThousandSeparator:=','; DateSeparator:='/'; TimeSeparator:=':' end;
  assignfile(f, 'data\log.txt'); rewrite(f); closefile(f); FontCheck(false, ['Ubuntu', 'Ubuntu Condensed', 'Segoe UI', 'Segoe UI Light']);
  try
  //
  selPl:=TStringList.Create; x:=TXMLVerySimple.Create; Sett:=TXMLVerySimple.Create;
  Years:=TIntList.Create; Years.Sorted:=true; Years.Duplicates:=dupIgnore;
  selYears:=TIntList.Create; selYears.Sorted:=true; selYears.Duplicates:=dupIgnore;
  //
  log('Reading data...');
  x.LoadFromFile('data\data.xml'); Sett.LoadFromFile('data\settings.xml');
  Tags:=x.Root.Find('tags'); if Tags.ChildNodes.Count=0 then MessageDlg('The tag list is empty! The app will continue, but will probably not work correctly.', mtError, [mbOK], 0);
  xl:=x.Root.Find('populations').FindNodes('place'); nPl:=xl.Count; setlength(pl, nPl);
  if nPl>0 then for i:=0 to nPl-1 do
    begin
      pl[i].Nume:=xl[i].Attribute['nume']; DecodeTags(xl[i].Attribute['tags'], pl[i].Tags); pl[i].Color:=HTMLtoTColor(xl[i].Attribute['color']);
      yl:=xl[i].FindNodes('pop'); pl[i].nPop:=yl.Count; setlength(pl[i].Populations, pl[i].nPop);
      if pl[i].nPop>0 then for j:=0 to yl.Count-1 do
        begin
          try pl[i].Populations[j].Year := strtoint(yl[j].Attribute['year']) except pl[i].Populations[j].Year:=0 end;
          try pl[i].Populations[j].Value := strtoint(yl[j].Attribute['value']) except pl[i].Populations[j].Value:=0 end
        end
      else MessageDlg('The population list for "'+pl[i].Nume+'" is empty. This may cause some errors.', mtWarning, [mbOK], 0);;
      log('Successfully read place "'+pl[i].Nume+'"')
    end;
  // X will not be freed; NOTE that Tags points to X !
  except on E:Exception do MessageDlg('ERROR while reading data'+dnl+e.classname+' :  '+e.Message, mtError, [mbOK], 0) end;
  log('Successfully read data!')
end;

end.

