object FSett: TFSett
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsSingle
  Caption = 'Settings'
  ClientHeight = 350
  ClientWidth = 638
  Color = 4868682
  Font.Charset = ANSI_CHARSET
  Font.Color = clSilver
  Font.Height = -16
  Font.Name = 'Ubuntu'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ScreenSnap = True
  PixelsPerInch = 96
  TextHeight = 20
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 546
    Height = 21
    Caption = 
      'Changes are automatically saved and applied when you close this ' +
      'window.'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object applySettB: TButton
    Left = 157
    Top = 280
    Width = 161
    Height = 49
    Caption = 'Apply now'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -24
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = applySettBClick
  end
  object closeB: TButton
    Left = 324
    Top = 280
    Width = 161
    Height = 49
    Caption = 'Close'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -24
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = closeBClick
  end
  object GroupBox1: TGroupBox
    Left = 24
    Top = 48
    Width = 161
    Height = 89
    TabOrder = 2
    object Label2: TLabel
      Left = 16
      Top = 16
      Width = 113
      Height = 20
      Caption = 'Chart line width'
    end
    object Label3: TLabel
      Left = 79
      Top = 45
      Width = 17
      Height = 20
      Caption = 'px'
    end
    object cb1: TComboBox
      Left = 24
      Top = 42
      Width = 49
      Height = 28
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Ubuntu'
      Font.Style = []
      ItemIndex = 0
      ParentFont = False
      TabOrder = 0
      Text = '1'
      Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5')
    end
  end
  object CheckBox1: TCheckBox
    Left = 424
    Top = 48
    Width = 113
    Height = 17
    Caption = 'Legen visible'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -15
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
  end
end
