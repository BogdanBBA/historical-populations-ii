object F1: TF1
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'F1'
  ClientHeight = 495
  ClientWidth = 772
  Color = 4868682
  Font.Charset = ANSI_CHARSET
  Font.Color = clSilver
  Font.Height = -16
  Font.Name = 'Ubuntu'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 20
  object chTL: TLabel
    Left = 620
    Top = 8
    Width = 43
    Height = 26
    Caption = 'chTL'
    Font.Charset = ANSI_CHARSET
    Font.Color = 6504477
    Font.Height = -21
    Font.Name = 'Signika Negative'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object chSTL: TLabel
    Left = 620
    Top = 40
    Width = 50
    Height = 24
    Caption = 'chSTL'
    Font.Charset = ANSI_CHARSET
    Font.Color = 10052397
    Font.Height = -19
    Font.Name = 'Signika Negative'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object axisL: TLabel
    Left = 620
    Top = 70
    Width = 26
    Height = 16
    Caption = 'axisL'
    Font.Charset = ANSI_CHARSET
    Font.Color = 5789784
    Font.Height = -11
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object legendL: TLabel
    Left = 620
    Top = 92
    Width = 51
    Height = 17
    Caption = 'legendL'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object bottomP: TPanel
    Left = 0
    Top = 384
    Width = 772
    Height = 81
    BevelOuter = bvNone
    TabOrder = 0
    OnResize = bottomPResize
    object logL: TLabel
      Left = 728
      Top = 2
      Width = 28
      Height = 16
      Alignment = taRightJustify
      Caption = 'logL'
      Font.Charset = ANSI_CHARSET
      Font.Color = clSilver
      Font.Height = -13
      Font.Name = 'Ubuntu Mono'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 224
      Top = -1
      Width = 38
      Height = 18
      Caption = 'Label1'
      Font.Charset = ANSI_CHARSET
      Font.Color = clSilver
      Font.Height = -13
      Font.Name = 'Ubuntu'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = -1
      Width = 38
      Height = 18
      Caption = 'Label2'
      Font.Charset = ANSI_CHARSET
      Font.Color = clSilver
      Font.Height = -13
      Font.Name = 'Ubuntu'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 8
      Top = 15
      Width = 38
      Height = 18
      Caption = 'Label3'
      Font.Charset = ANSI_CHARSET
      Font.Color = clSilver
      Font.Height = -13
      Font.Name = 'Ubuntu'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object exitB: TButton
      Left = 595
      Top = 24
      Width = 161
      Height = 57
      Caption = 'Exit'
      Font.Charset = ANSI_CHARSET
      Font.Color = clSilver
      Font.Height = -27
      Font.Name = 'Ubuntu'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = Exit1Click
    end
    object minTB: TTrackBar
      Left = 214
      Top = 23
      Width = 300
      Height = 33
      SelEnd = 10
      TabOrder = 1
      ThumbLength = 16
      OnChange = minTBChange
    end
    object maxTB: TTrackBar
      Left = 214
      Top = 54
      Width = 300
      Height = 33
      Position = 10
      SelEnd = 10
      TabOrder = 2
      ThumbLength = 16
      OnChange = maxTBChange
    end
  end
  object tagCH: TCheckListBox
    Left = 8
    Top = 8
    Width = 200
    Height = 97
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -15
    Font.Name = 'Ubuntu'
    Font.Style = []
    ItemHeight = 19
    Items.Strings = (
      'test1'
      'asdjkdasnjm')
    ParentFont = False
    TabOrder = 1
    OnClick = tagCHClick
  end
  object plCH: TCheckListBox
    Left = 8
    Top = 136
    Width = 200
    Height = 97
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -15
    Font.Name = 'Ubuntu'
    Font.Style = []
    HeaderBackgroundColor = clWhite
    ItemHeight = 19
    Items.Strings = (
      'df'
      'dfgdfg'
      'fgdgffdgdf')
    ParentFont = False
    PopupMenu = plChPM
    Sorted = True
    TabOrder = 2
    OnClick = plCHClick
  end
  object ch: TChart
    Left = 214
    Top = 8
    Width = 400
    Height = 250
    SubTitle.Alignment = taRightJustify
    SubTitle.CustomPosition = True
    SubTitle.Left = 161
    SubTitle.Top = 2
    Title.Text.Strings = (
      'Historical Populations II')
    OnClickSeries = chClickSeries
    BottomAxis.Title.Caption = 'Year'
    LeftAxis.Title.Caption = 'Population'
    View3D = False
    View3DWalls = False
    Color = clWhite
    TabOrder = 3
    ColorPaletteIndex = 13
    object Series1: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      Pointer.Brush.Gradient.EndColor = 10708548
      Pointer.Gradient.EndColor = 10708548
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object Series2: THorizBarSeries
      BarBrush.Gradient.Direction = gdLeftRight
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      Emboss.Color = 8487297
      Gradient.Direction = gdLeftRight
      Shadow.Color = 8487297
      XValues.Name = 'Bar'
      XValues.Order = loNone
      YValues.Name = 'Y'
      YValues.Order = loAscending
    end
  end
  object MainMenu1: TMainMenu
    Left = 136
    Top = 16
    object App1: TMenuItem
      Caption = 'App'
      object About1: TMenuItem
        Caption = 'About'
        OnClick = About1Click
      end
      object Settings1: TMenuItem
        Caption = 'Settings'
        OnClick = Settings1Click
      end
      object Exit1: TMenuItem
        Caption = 'Exit'
        OnClick = Exit1Click
      end
    end
    object Actions1: TMenuItem
      Caption = 'Actions'
      Visible = False
      object fullRefresh1: TMenuItem
        Caption = 'fullRefresh'
        OnClick = fullRefresh1Click
      end
    end
  end
  object plChPM: TPopupMenu
    Left = 120
    Top = 208
    object Select21: TMenuItem
      Caption = 'Select 2'
      OnClick = Select21Click
    end
    object Select51: TMenuItem
      Caption = 'Select 5'
      OnClick = Select21Click
    end
    object Select101: TMenuItem
      Caption = 'Select 10'
      OnClick = Select21Click
    end
    object Select201: TMenuItem
      Caption = 'Select 20'
      OnClick = Select21Click
    end
    object Selectall1: TMenuItem
      Caption = 'Select ALL'
      OnClick = Selectall1Click
    end
    object Selectnone1: TMenuItem
      Caption = 'Select NONE'
      OnClick = Selectnone1Click
    end
    object Selectrandomly1: TMenuItem
      Caption = 'Select randomly'
      OnClick = Selectrandomly1Click
    end
  end
  object refT: TTimer
    Enabled = False
    Interval = 600
    OnTimer = refTTimer
    Left = 176
    Top = 208
  end
end
