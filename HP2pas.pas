unit HP2pas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus,
  Vcl.StdCtrls, Vcl.ExtCtrls, VCLTee.TeEngine, VCLTee.TeeProcs, VCLTee.Chart,
  Vcl.CheckLst, Vcl.ComCtrls, VCLTee.Series;

type
  TF1 = class(TForm)
    MainMenu1: TMainMenu;
    App1: TMenuItem;
    About1: TMenuItem;
    Settings1: TMenuItem;
    Exit1: TMenuItem;
    bottomP: TPanel;
    exitB: TButton;
    logL: TLabel;
    tagCH: TCheckListBox;
    plCH: TCheckListBox;
    ch: TChart;
    Actions1: TMenuItem;
    fullRefresh1: TMenuItem;
    plChPM: TPopupMenu;
    Select21: TMenuItem;
    Select51: TMenuItem;
    Select101: TMenuItem;
    Select201: TMenuItem;
    Selectall1: TMenuItem;
    Selectrandomly1: TMenuItem;
    refT: TTimer;
    minTB: TTrackBar;
    maxTB: TTrackBar;
    Label1: TLabel;
    Selectnone1: TMenuItem;
    Label2: TLabel;
    Label3: TLabel;
    chTL: TLabel;
    chSTL: TLabel;
    Series1: TLineSeries;
    Series2: THorizBarSeries;
    axisL: TLabel;
    legendL: TLabel;
    procedure TBChange(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure bottomPResize(Sender: TObject);
    procedure fullRefresh1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure tagCHClick(Sender: TObject);
    procedure Selectrandomly1Click(Sender: TObject);
    procedure refTTimer(Sender: TObject);
    procedure plCHClick(Sender: TObject);
    procedure Selectall1Click(Sender: TObject);
    procedure Select21Click(Sender: TObject);
    procedure minTBChange(Sender: TObject);
    procedure maxTBChange(Sender: TObject);
    procedure Settings1Click(Sender: TObject);
    procedure Selectnone1Click(Sender: TObject);
    procedure chTCBChange(Sender: TObject);
    procedure chClickSeries(Sender: TCustomChart; Series: TChartSeries;
      ValueIndex: Integer; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F1: TF1;

implementation

{$R *.dfm}

uses DataTypes, FunctiiBaza, Settings;

procedure TF1.About1Click(Sender: TObject);
begin
  MessageDlg('by BogdyBBA'+dnl+'v2.0 alpha - September 11th, 2012'+nl+'v2.0 beta - September 12th, 2012', mtInformation, [mbOK], 0)
end;

procedure TF1.bottomPResize(Sender: TObject);
begin
  exitB.Left:=bottomP.Width-exitB.Width-24; logL.Left:=bottomP.Width-logL.Width-24
end;

procedure TF1.chClickSeries(Sender: TCustomChart; Series: TChartSeries;
  ValueIndex: Integer; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  showmessage(Series.Title)
end;

procedure TF1.chTCBChange(Sender: TObject);
begin
  refT.Enabled:=false; refT.Enabled:=true
end;

procedure TF1.Exit1Click(Sender: TObject);
begin
  Halt
end;

procedure TF1.FormCreate(Sender: TObject);
begin
  ReadData;
end;

procedure TF1.FormResize(Sender: TObject);
var h: word;
begin
  bottomP.Width:=F1.Width; bottomP.Top:=F1.Height-bottomP.Height-32;
  ch.Width:=F1.Width-ch.Left-8; ch.Height:=F1.Height-bottomP.Height-ch.Top-32;
  h:=bottomP.Top-tagCH.Top; tagCH.Height:=h div 3-6; plCH.Top:=h div 3+6; plCH.Height:=ch.Top+ch.Height-plCH.Top;
end;

procedure TF1.Select21Click(Sender: TObject);
var i, xn: word;
begin
  if plCH.Items.Count=0 then exit;
  try xn:=strtoint(copy(TMenuItem(Sender).Caption, pos(' ', TMenuItem(Sender).Caption)+1, 2)) except xn:=1 end;
  for i:=0 to plCH.Items.Count-1 do plCH.Checked[i]:=i<xn;
  plCHClick(Sender)
end;

procedure TF1.Selectall1Click(Sender: TObject);
var i: word;
begin
  if plCH.Items.Count=0 then exit;
  for i:=0 to plCH.Items.Count-1 do plCH.Checked[i]:=true;
  plCHClick(Selectall1)
end;

procedure TF1.Selectnone1Click(Sender: TObject);
var i: word;
begin
  if plCH.Items.Count=0 then exit;
  for i:=0 to plCH.Items.Count-1 do plCH.Checked[i]:=false;
  plCHClick(Selectnone1)
end;

procedure TF1.Selectrandomly1Click(Sender: TObject);
var i: word;
begin
  if plCH.Items.Count=0 then exit;
  for i:=0 to plCH.Items.Count-1 do plCH.Checked[i]:=inttoboolean(random(2));
  plCHClick(Selectrandomly1)
end;

procedure TF1.Settings1Click(Sender: TObject);
begin
  if FSett.Showing then FSett.OnShow(Settings1) else FSett.Show
end;

procedure TF1.FormShow(Sender: TObject);
begin
  fullRefresh1Click(F1);
  tagCH.Checked[strtoint(Sett.Root.Find('StartupTagii').Attribute['value'])]:=true; tagCHClick(F1)
end;

//

procedure TF1.fullRefresh1Click(Sender: TObject);
var i: word;
begin
  //Please keep the IDs of tags to be the position (starting at 1); otherwise, complications may occur
  tagCH.Clear; for i:=0 to Tags.ChildNodes.Count-1 do tagCH.Items.Add(Tags.ChildNodes[i].Text);
  tagCHClick(fullRefresh1)
end;

procedure TF1.TBChange(Sender: TObject);
var xs: string; i: word; smtChd: boolean;
begin
  if PlCH.Items.Count=0 then exit; smtChd:=false; for i:=0 to PlCH.Items.Count-1 do if PlCH.Checked[i] then begin smtChd:=true; break end;
  if (minTB.Position>maxTB.Position) or not smtChd then begin xs:='There are no years to be filtered. '; exit end;
  if minTB.Position=maxTB.Position then xs:='Filter by year '+inttostr(Years[minTB.Position])+' '
  else xs:='Filter by year range '+inttostr(Years[minTB.Position])+'-'+inttostr(Years[maxTB.Position])+' ';
  Label1.Caption:=xs
end;

procedure TF1.maxTBChange(Sender: TObject);
begin
  maxTB.SelEnd:=maxTB.Position; if maxTB.Position<minTB.Position then minTB.Position:=maxTB.Position;
  TBChange(maxTB)
end;

procedure TF1.minTBChange(Sender: TObject);
begin
  minTB.SelStart:=minTB.Position; if minTB.Position>maxTB.Position then maxTB.Position:=minTB.Position;
  TBChange(minTB)
end;

procedure TF1.plCHClick(Sender: TObject);
begin
  refT.Enabled:=false; refT.Enabled:=true
end;

procedure TF1.tagCHClick(Sender: TObject);
var i, j, nT, nPP: word;
begin
  nT:=0; nPP:=0; plCH.Clear; //for each selected tag, add who has that tag and isn't already in the list
  for i:=0 to tagCH.Items.Count-1 do
    if tagCH.Checked[i] then
      begin
        inc(nT);
        for j:=0 to nPl-1 do
          if (pl[j].Tags.IndexOf(inttostr(i))<>-1) and (plCH.Items.IndexOf(pl[j].Nume)=-1) then
            begin inc(nPP); plCH.Items.Add(pl[j].Nume) end
      end;
  Label2.Caption:='Filter by '+inttostr(nT)+' / '+inttostr(tagCH.Items.Count)+' tags ';
  Label3.Caption:='Places found: '+inttostr(nPP)+' ';
  F1.Select21Click(Select21)
end;

procedure TF1.refTTimer(Sender: TObject);
var i, j: word; cs: TLineSeries;
begin
  refT.Enabled:=false;
    if plCH.Items.Count=0 then exit;
  selPl.Clear; for i:=0 to plCH.Items.Count-1 do if plCH.Checked[i] then selPl.Add(plCH.Items[i]);
    if selPl.Count=0 then begin minTB.Max:=minTB.Min; maxTB.Max:=maxTB.Min; Label1.Caption:='There are no years to be filtered. '; exit end;
  Years.Clear; for i:=0 to selPl.Count-1 do for j:=0 to pl[pozPl(selPl[i])].nPop-1 do
    Years.Add(pl[pozPl(selPl[i])].Populations[j].Year);
  minTB.Max:=Years.Count-1; maxTB.Max:=minTB.Max;
  if Sett.Root.Find('RefreshAutoResetYearRange').Attribute['value']='1' then begin minTB.Position:=0; maxTB.Position:=maxTB.Max end;
  TBChange(refT);
  //
  ch.ClearChart;
  ch.View3D:=false;
  ch.Title.Font.Assign(chTL.Font); ch.Title.Text.Add('Historical Populations II');
  //ch.SubTitle.Font.Assign(chSTL.Font);
  ch.LeftAxis.LabelsFont.Assign(axisL.Font); ch.BottomAxis.LabelsFont.Assign(axisL.Font);
  ch.Legend.Font.Assign(legendL.Font); ch.Legend.Visible:=false;
  ch.BottomAxis.Title.Font.Assign(legendL.Font); ch.BottomAxis.Title.Caption:='Year (AD)';
  ch.LeftAxis.Title.Font.Assign(legendL.Font); ch.LeftAxis.Title.Caption:='Population';
  //
  for i:=0 to selPL.Count-1 do
    begin
      cs:=TLineSeries.Create(Self); cs.Pen.Width:=strtoint(Sett.Root.Find('LineWidth').Attribute['value']);
      cs.Title:=pl[pozPl(selPl[i])].Nume;

      with pl[pozPl(selPl[i])] do
        begin
          for j:=0 to nPop-1 do
            if (Years.IndexOf(Populations[j].Year)>=minTB.Position) and (Years.IndexOf(Populations[j].Year)<=maxTB.Position) then
              cs.AddXY(Populations[j].Year, Populations[j].Value, inttostr(Populations[j].Year), Color);
          ch.AddSeries(cs)
        end;
    end;
end;

end.
